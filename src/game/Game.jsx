import "../styling/App.css";
import "../styling/Minecraft.css";
// import Helmet from 'react-helmet'
import minecraft from "../images/Minecraft-new.svg";
import music from "../music/song.mp3";
import { options, quit, how, playaudio } from "./Buttons/gamemod";
import { play, playmulti } from "./Buttons/gamemine";
// import { updateidea, spawnNotification } from './Buttons/audio'

export default function Game() {
  const credits = () => {
    alert("Thanks to classic.minecraft.net for the game part");
  };
  return (
    <>
      <img alt="Minecraft" id="logo" src={minecraft} />
      <div id="flashingtext">PLAY...PLAY...PLAY</div>
      <main>
        <button className="button" onClick={play}>
          Singleplayer
        </button>
        <br />
        <button className="button" onClick={playmulti} id="playmulti">
          Multiplayer
        </button>{" "}
        <br />
        <button className="button" onClick={credits}>
          Credits
        </button>
        <button className="button" onClick={how}>
          How to play
        </button>
        <audio id="audio">
          <source src={music} />
        </audio>
        <button id="button" className="button" onClick={playaudio}>
          Play audio
        </button>
        <button
          className="button button_small left"
          onClick={options}
          style={{ width: "49%" }}
        >
          Options
        </button>
        <button
          className="button button_small right"
          style={{ width: "49%" }}
          onClick={quit}
        >
          Quit Game
        </button>
      </main>
    </>
  );
}
