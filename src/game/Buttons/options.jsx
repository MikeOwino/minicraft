export default function options() {
  const options = () => {
    addhtml(
      "<div style='position:fixed; top:0; left:0; text-align:center; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:1.5; color:white; background-color:black; font-size:125%;' id='options'>Do you want to <button class='button'onclick='change()'>Change the flashing text</button><button onclick='openwindowform()' class='button'>Open window form</button><button onclick='enablenarrations()' id='en' class='button'>Enable narrations</button><button onclick='closeoptions()' class='button'>Close options</button></div>"
    );
  };
  return options(<></>);
}
