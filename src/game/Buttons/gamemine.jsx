import {
  options,
  enablenarrations,
  change,
  openwindowform,
  closeoptions,
  quit,
  how,
} from "./gamemod";

const remove = (elementId) => {
  var element = document.getElementById(elementId);
  element.parentNode.removeChild(element);
};

const play = () => {
  addhtml(
    "<iframe src='https://classic.minecraft.net' id='game'></iframe><div id='X' style='color: red; position:absolute; right:0; top:0; font-weight: bold; font-size: 70px;z-index: 2;cursor: pointer;' onclick='stop();'>x</div><div onclick='stop();' style='position:fixed; top:0; left:0; text-align:center; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:1.5; color:white; background-color:black; font-size:165%;' id='loading'>LOADING...</div>"
  );
};

const stophow = () => {
  remove("instructions");
  remove("stophow");
};

const stop = () => {
  remove("game");
  remove("X");
  remove("loading");
};

const stopmulti = () => {
  remove("game");
  remove("X");
  remove("blahblah");
  remove("loading");
};

const nomulti = () => {
  remove("nomulti");
  remove("blahblah");
};

const playmulti = () => {
  addhtml(
    "<div id='blahblah' style='position: absolute; margin: auto; top: 0; right: 0; bottom: 0; left: 0;'><input style='width: 90%; outline: none;' type='text' placeholder='multyplayer game url add https' id='multyurl' autocomplete='off' /><input type='button' onclick='startmultigame()' value='summit' /></div><div id='nomulti' style='color: red; position:absolute; left:0; bottom:0; font-weight: bold; font-size: 70px;z-index: 2;cursor: pointer;' onclick='nomulti();'>x</div>"
  );
};

const startmultigame = () => {
  remove("nomulti");
  var multi = document.getElementById("multyurl").value;
  addhtml(
    "<iframe src=" +
      multi +
      " id='game'></iframe><div id='X' style='color: red; position:absolute; right:0; top:0; font-weight: bold; font-size: 70px;z-index: 2;cursor: pointer;' onclick='stopmulti();'>x</div><div onclick='stop();' style='position:fixed; top:0; left:0; text-align:center; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:1.5; color:white; background-color:black; font-size:165%;' id='loading'>LOADING...</div> "
  );
  remove("audio");
};

const addhtml = (html) => {
  document.body.innerHTML = document.body.innerHTML + html;
};

const say = (stuff) => {
  var speak = new SpeechSynthesisUtterance(stuff);
  speechSynthesis.speak(speak);
};
export {
  play,
  stophow,
  stop,
  stopmulti,
  nomulti,
  playmulti,
  startmultigame,
  remove,
  addhtml,
  say,
};
