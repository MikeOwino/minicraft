const updateidea = () => {
  if (!("Notification" in window)) {
    alert("This browser does not support desktop notification");
  } else if (Notification.permission === "granted") {
    var notification = new Notification(
      "updates? https://repl.it/talk/share/Minecraft/16676 I am duck1321912"
    );
  } else if (Notification.permission !== "denied") {
    Notification.requestPermission().then(function (permission) {
      if (permission === "granted") {
        var notification = new Notification(
          "updates? https://repl.it/talk/share/Minecraft/16676"
        );
      }
    });
  }
};
Notification.requestPermission().then(function (result) {
  console.log(result);
});

const spawnNotification = (body, icon, title) => {
  var options = {
    body: body,
    icon: icon,
  };
};
var button = document.getElementById("button");
var audio = document.getElementById("audio");

button.addEventListener("click", function () {
  if (audio.paused) {
    audio.play();
    button.innerHTML = "Pause audio";
  } else {
    audio.pause();
    button.innerHTML = "Play audio";
  }
});

export { updateidea, spawnNotification };
